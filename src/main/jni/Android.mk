# Build Ovusense & wrapper
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := Ovusense
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)
LOCAL_SRC_FILES := ../obj/local/$(TARGET_ARCH_ABI)/libOvusense.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := OvusenseWrapper
LOCAL_STATIC_LIBRARIES := Ovusense
LOCAL_SRC_FILES := OvusenseWrapper.c
LOCAL_CFLAGS += -std=c99
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)
include $(BUILD_SHARED_LIBRARY)
