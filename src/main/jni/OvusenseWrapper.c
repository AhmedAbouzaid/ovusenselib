﻿/*
 * This file is recompiled with the project
 */

#include <string.h>
#include <jni.h>
#include <Ovusense.h>

jbyteArray as_byte_array(JNIEnv *env, unsigned char* buf, int len) {
    jbyteArray array = (*env)->NewByteArray (env, len);
    (*env)->SetByteArrayRegion (env, array, 0, len, (jbyte*)buf);
    return array;
}

jintArray as_jint_array(JNIEnv *env, int* buf, int len) {
	jintArray array = (*env)->NewIntArray (env, len);
	(*env)->SetIntArrayRegion (env, array, 0, len, buf);
	return array;
}

jcharArray as_jchar_array(JNIEnv *env, unsigned short* buf, int len) {
	jcharArray array = (*env)->NewCharArray (env, len);
	(*env)->SetCharArrayRegion (env, array, 0, len, buf);
	return array;
}

unsigned char* as_unsigned_char_array(JNIEnv *env, jbyteArray array) {
    int len = (*env)->GetArrayLength (env, array);
    unsigned char* buf = (unsigned char *)calloc((size_t)len, sizeof(unsigned char));
    (*env)->GetByteArrayRegion (env, array, 0, len, (jbyte*)buf);
    return buf;
}

unsigned short* unsigned_short_from_array_region(JNIEnv *env, jcharArray array, jint index, jint length) {
	int start = index * length;
	unsigned short* buf = (unsigned short *)calloc((size_t)length, sizeof(unsigned short));
	(*env)->GetCharArrayRegion (env, array, start, length, buf);
	return buf;
}

int* as_int_array(JNIEnv *env, jintArray array) {
	int len = (*env)->GetArrayLength (env, array);
	int* buf = (int *)calloc((size_t)len, sizeof(int));
	(*env)->GetIntArrayRegion (env, array, 0, len, buf);
	return buf;
}

int* int_from_array_region(JNIEnv *env, jintArray array, jint index, jint length) {
	int start = index * length;
	int* buf = (int *)calloc((size_t)length, sizeof(int));
	(*env)->GetIntArrayRegion (env, array, start, length, buf);
	return buf;
}

/*
 * OvusenseAsyncCalculation Methods
 */

jint Java_com_fertilityfocus_ovusenseapi_ovusense_OvusenseAsyncCalculation_AnalyseCycle(JNIEnv *env, jobject thiz, jintArray jCycleStart, jbyteArray jPreviousOvDay, jintArray jRecordingsStarts, jcharArray Ts) {

	jclass cls = (*env)->GetObjectClass(env, thiz);

	jmethodID mid2 = (*env)->GetMethodID(env, cls, "analyseDataFinished", "([IC[C[CI)V");
	if (mid2 == 0) return -2;

	hw_time_t nativeCycleStart;
	memset(&nativeCycleStart, 0, sizeof(nativeCycleStart));
	int* timeArray1 = as_int_array(env, jCycleStart);
	nativeCycleStart.year = (unsigned short)timeArray1[0];
	nativeCycleStart.month = (unsigned char)timeArray1[1];
	nativeCycleStart.daynum = (unsigned char)timeArray1[2];

	int startIndex = 0;
	int regionLength = (*env)->GetArrayLength (env, jPreviousOvDay);
	if (regionLength>MAXCYCLES) {
		startIndex = regionLength - MAXCYCLES;
		regionLength = MAXCYCLES;
	}
	unsigned char PreviousOvDay[MAXCYCLES];
	memset(PreviousOvDay, 0, sizeof(PreviousOvDay));
	(*env)->GetByteArrayRegion(env, jPreviousOvDay, startIndex, regionLength, (jbyte*)PreviousOvDay);

	unsigned char AlgState[512];
	memset(&AlgState, 0, sizeof(AlgState));

	unsigned char predictedOvDay = 0;

	PredictOvDay(&nativeCycleStart, PreviousOvDay, &predictedOvDay, AlgState);

	int len = ((*env)->GetArrayLength (env, jRecordingsStarts)) / 3;

	if (len==0) {
		int returnDateArray[3] = {0, 0, 0};
		unsigned char returnStatus = 0;
		unsigned short graphData[40];
		memset(graphData, 0, sizeof(graphData));
		jintArray returnDateJIntArray = as_jint_array(env, returnDateArray, sizeof(returnDateArray) / sizeof(returnDateArray[0]));
		jcharArray graphDataJCharArray = as_jchar_array(env, graphData, sizeof(graphData) / sizeof(graphData[0]));
		(*env)->CallVoidMethod(env, thiz, mid2, returnDateJIntArray, returnStatus, graphDataJCharArray, graphDataJCharArray, predictedOvDay);
		return -1;
	}

	for (int i=0; i<len; i++) {

		hw_time_t returnDate;
		memset(&returnDate, 0, sizeof(returnDate));
		unsigned char returnStatus = 0;
		unsigned short graphData[40];
		memset(graphData, 0, sizeof(graphData));
		unsigned short smoothData[40];
		memset(smoothData, 0, sizeof(smoothData));

		hw_time_t nativeRecStart;
		memset(&nativeRecStart, 0, sizeof(nativeRecStart));

		int *timeArray2 = int_from_array_region(env, jRecordingsStarts, i, 3);
		nativeRecStart.year = (unsigned short)timeArray2[0];
		nativeRecStart.month = (unsigned char)timeArray2[1];
		nativeRecStart.daynum = (unsigned char)timeArray2[2];

		unsigned short *Temperatures = unsigned_short_from_array_region(env, Ts, i, 164);

		AnalyseData(&nativeRecStart, Temperatures, &returnDate, &returnStatus, graphData, AlgState, smoothData);

		int returnDateArray[3] = {returnDate.year, returnDate.month, returnDate.daynum};
		jintArray returnDateJIntArray = as_jint_array(env, returnDateArray, sizeof(returnDateArray) / sizeof(returnDateArray[0]));
		jcharArray graphDataJCharArray = as_jchar_array(env, graphData, sizeof(graphData) / sizeof(graphData[0]));
		jcharArray smoothDataJCharArray = as_jchar_array(env, smoothData, sizeof(smoothData) / sizeof(smoothData[0]));
		(*env)->CallVoidMethod(env, thiz, mid2, returnDateJIntArray, returnStatus, graphDataJCharArray, smoothDataJCharArray, predictedOvDay);
	}

	return -1;

}

jbyte Java_com_fertilityfocus_ovusenseapi_ovusense_OvusenseAsyncCalculation_PredictOvDay(JNIEnv *env, jobject thiz, jintArray jCycleStart, jbyteArray jPreviousOvDay, jbyteArray jAlgState) {

	hw_time_t nativeCycleStart;
	memset(&nativeCycleStart, 0, sizeof(nativeCycleStart));
	int* timeArray = as_int_array(env, jCycleStart);
	nativeCycleStart.year = (unsigned short)timeArray[0];
	nativeCycleStart.month = (unsigned char)timeArray[1];
	nativeCycleStart.daynum = (unsigned char)timeArray[2];

	unsigned char* PreviousOvDay = as_unsigned_char_array(env, jPreviousOvDay);
	unsigned char* AlgState = as_unsigned_char_array(env, jAlgState);
	unsigned char predictedOvDay;

	PredictOvDay(&nativeCycleStart, PreviousOvDay, &predictedOvDay, AlgState);

	jclass cls = (*env)->GetObjectClass(env, thiz);
	jmethodID mid = (*env)->GetMethodID(env, cls, "updateAlgState", "([B)V");
	if (mid == 0) return 0;
	jbyteArray algStateByteArray = as_byte_array(env, AlgState, sizeof(AlgState) / sizeof(AlgState[0]));
	(*env)->CallVoidMethod(env, thiz, mid, algStateByteArray);
	return predictedOvDay;

}

jstring Java_com_fertilityfocus_ovusenseapi_ovusense_OvusenseAsyncCalculation_testWrapper(JNIEnv *env, jobject thiz) {
	jclass cls = (*env)->GetObjectClass(env, thiz);
	jmethodID jlog = (*env)->GetMethodID(env, cls, "logFromJNI", "(Ljava/lang/String;)V");

	if (jlog != 0) {
		char str[500] = "TEST CALCULATION OUT";
		jstring message = (*env)->NewStringUTF(env, str);
		(*env)->CallVoidMethod(env, thiz, jlog, message);
	}

	return (*env)->NewStringUTF(env, "Hello from JNI !");
}