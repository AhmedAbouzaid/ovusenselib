//----------------------------------------------------------------------------------------------------------------------------------------
// filename:    OvuSense.h
// author:      James Pardey for Fertility Focus Ltd
// description: header file for OvuSense.cpp
//----------------------------------------------------------------------------------------------------------------------------------------

#define _CRT_SECURE_NO_DEPRECATE

#include <math.h>
#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define	STRING                     12                       // date string length, including the trailing '\0'
#define	MAXCYCLES                  12                       // maximum number of previous cycles to use for Ov prediction
#define	EXPECTED                    0x80                    // ovulation expected
#define	CONFIRMED                   0x40                    // ovulation confirmed

#define	RELEASE                     1                       // build a release version

// #define MIN_TEMP             35500                       // current sensor minimum output is 35.5C
#define MIN_TEMP                34500                       // NFC sensor minimum output is 34.5C
#define PAYLOAD_SIZE              246                       // maximum payload size in bytes
#define MAX_ANALYSE_WINDOW         (2 * PAYLOAD_SIZE / 3)   // maximum recording size to analyse is 13 hours 40 minutes
#define SUCCESS                   255                       //
#define SHORT_RECORDING            47                       // The recording is too short to analyse
#define GRAPH_MAX_DAYS             40                       // Added

#pragma pack(1)
typedef struct {
    unsigned char             second;                       // 0,...,59
    unsigned char             minute;                       // 0,...,59
    unsigned char             hour;                         // 0,...,23
    unsigned char             daynum;                       // 1,...,31
    unsigned char             month;                        // 1,...,12
    unsigned char             weekday;                      // 0,....,6 where 0 = Sunday
    unsigned short            year;                         // 2000,...
    unsigned char             powerfail;                    //
} hw_time_t;
#pragma pack()

int PredictOvDay(hw_time_t *CycleStart, unsigned char PreviousOvDay[], unsigned char *PredictedOvDay, unsigned char *AlgState);
int AnalyseData(hw_time_t *RecordingStart, unsigned short *T, hw_time_t *ReturnDate, unsigned char *ReturnStatus, unsigned short *GraphData, unsigned char *AlgState, unsigned short *SmoothGraphData);