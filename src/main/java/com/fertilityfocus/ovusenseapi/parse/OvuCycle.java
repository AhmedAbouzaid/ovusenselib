package com.fertilityfocus.ovusenseapi.parse;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Al Pirrie on 03/2018.
 */

@ParseClassName("OvuCycle")
public class OvuCycle extends ParseObject {

    public static final String CYCLE_TYPE_IVF = "ivf";
    public static final String CYCLE_TYPE_IUI = "iui";
    public static final String CYCLE_TYPE_NATURAL = "natural";
    public static final String CYCLE_TYPE_MEDICATED = "medicated";

    public String getOwnerUserID() {
        return getString("ownerUserID");
    }
    public void setOwnerUserID(String value) {
        put("ownerUserID", value);
    }

    public Date getStartDateTimeUTC() {
        return getDate("startDateTimeUTC");
    }
    public void setStartDateTimeUTC(Date value) {
        put("startDateTimeUTC", value);
    }

    public String getUuid() {
        return getString("uuid");
    }
    public void setUuid(String value) {
        put("uuid", value);
    }

    public String getTags() {
        return getString("tags");
    }
    public void setTags(String value) {
        put("tags", value);
    }

    public String getOvulationMessage() {
        return getString("ovulationMessage");
    }
    public void setOvulationMessage(String value) {
        put("ovulationMessage", value);
    }

    public Date getLastCalculation() {
        return getDate("lastCalculation");
    }
    public void setLastCalculation(Date value) {
        if (value == null) {
            remove("lastCalculation");
        } else {
            put("lastCalculation", value);
        }
    }

    public long getOvulationDay() {
        return getLong("ovulationDay");
    }
    public void setOvulationDay(long value) {
        put("ovulationDay", value);
    }

    public long getOvExpectedDay() {
        return getLong("OvExpectedDay");
    }
    public void setOvExpectedDay(long value) {
        put("OvExpectedDay", value);
    }

    public boolean getShowFertileArea() {
        return getBoolean("showFertileArea");
    }
    public void setShowFertileArea(boolean value) {
        put("showFertileArea", value);
    }

    public boolean getShowOvulatingArea() {
        return getBoolean("showOvulatingArea");
    }
    public void setShowOvulatingArea(boolean value) {
        put("showOvulatingArea", value);
    }

    public boolean getShowFertileIndicator() {
        return getBoolean("showFertileIndicator");
    }
    public void setShowFertileIndicator(boolean value) {
        put("showFertileIndicator", value);
    }

    public boolean getShowOvulatingIndicator() {
        return getBoolean("showOvulatingIndicator");
    }
    public void setShowOvulatingIndicator(boolean value) {
        put("showOvulatingIndicator", value);
    }

    public boolean getOvAllowShift() {
        return getBoolean("ovAllowShift");
    }
    public void setOvAllowShift(boolean value) {
        put("ovAllowShift", value);
    }

    public boolean getHidden() {
        return getBoolean("hidden");
    }
    public void setHidden(boolean value) {
        put("hidden", value);
    }

    public boolean getNeedsRecalculating() {
        return getBoolean("needsRecalculating");
    }
    public void setNeedsRecalculating(boolean value) {
        put("needsRecalculating", value);
    }

    public boolean getShowOvulationLine() {
        return getBoolean("showOvulationLine");
    }
    public void setShowOvulationLine(boolean value) {
        put("showOvulationLine", value);
    }

    public boolean getOvulationNotConfirmed() {
        return getBoolean("ovulationNotConfirmed");
    }
    public void setOvulationNotConfirmed(boolean value) {
        put("ovulationNotConfirmed", value);
    }

    public boolean getHasCalculatedSmoothResults() { return getBoolean("hasCalculatedSmoothResults"); }
    public void setHasCalculatedSmoothResults(boolean value) { put("hasCalculatedSmoothResults", value); }

    public boolean getContainsSmoothData() {
        return getBoolean("containsSmoothData");
    }
    public void setContainsSmoothData(boolean value) {
        put("containsSmoothData", value);
    }

    public boolean getNeedsSyncing() {
        return getBoolean("needsSyncing");
    }
    public void setNeedsSyncing(boolean value) {
        put("needsSyncing", value);
    }

    /*
     * Quick setter
     */
    public static OvuCycle create(String msg, Date start, int ovDay, boolean showOvArea, boolean showFertArea, boolean showOvLine) {
        OvuCycle oc = new OvuCycle();
        oc.setOvulationMessage(msg);
        oc.setStartDateTimeUTC(start);
        oc.setOvulationDay(ovDay);
        oc.setShowOvulatingArea(showOvArea);
        oc.setShowFertileArea(showFertArea);
        oc.setShowOvulationLine(showOvLine);
        return oc;
    }

    public void clone(OvuCycle c) {
        if (c==null) return;
        if (c.getOwnerUserID()!=null) this.setOwnerUserID( "CLONE" + c.getOwnerUserID() );
        if (c.getStartDateTimeUTC()!=null) this.setStartDateTimeUTC( c.getStartDateTimeUTC() );
        if (c.getUuid()!=null) this.setUuid( c.getUuid() );
        if (c.getTags()!=null) this.setTags( c.getTags() );
        if (c.getOvulationMessage()!=null) this.setOvulationMessage( c.getOvulationMessage() );
        if (c.getLastCalculation()!=null) this.setLastCalculation( c.getLastCalculation() );
        this.setOvulationDay( c.getOvulationDay() );
        this.setOvExpectedDay( c.getOvExpectedDay() );
        this.setShowFertileArea( c.getShowFertileArea() );
        this.setShowOvulatingArea( c.getShowOvulatingArea() );
        this.setShowFertileIndicator( c.getShowFertileIndicator() );
        this.setShowOvulatingIndicator( c.getShowOvulatingIndicator() );
        this.setOvAllowShift( c.getOvAllowShift() );
        this.setHidden( c.getHidden() );
        this.setNeedsRecalculating( c.getNeedsRecalculating() );
        this.setShowOvulationLine( c.getShowOvulationLine() );
        this.setOvulationNotConfirmed( c.getOvulationNotConfirmed() );
        this.setHasCalculatedSmoothResults( c.getHasCalculatedSmoothResults() );
        this.setContainsSmoothData( c.getContainsSmoothData() );
        this.setNeedsSyncing( c.getNeedsSyncing() );
    }

    public static String debugList(List<OvuCycle> list) {
        if (list==null || list.size()==0) return "[]";
        StringBuilder result = new StringBuilder("[\n");
        for (OvuCycle c : list) {
            result.append( c.debug() );
            result.append( "\n" );
        }
        result.append( "]" );
        return result.toString();
    }

    public String debug() {
        String result = this.getStartDateTimeUTC() + " :";
        result += " TAGS:" + this.getTags();
        result += " OWNER:" + this.getOwnerUserID();
        return result;
    }

    public boolean hasTag(String tag) {
        List tags = getTagList();
        return tags.contains(tag);
    }

    private void updateTags(List<String> add, List<String> remove) {
        List<String> tags = getTagList();
        boolean madeChanges = false;
        for (String a : add) if (!tags.contains(a)) { tags.add(a); madeChanges=true; }
        for (String r : remove) { tags.remove(r); madeChanges=true; }
        StringBuilder tagsString = new StringBuilder();
        for (String t : tags) { tagsString.append(t); tagsString.append("|"); }
        // Remove last "|"
        setTags( tagsString.substring(0,tagsString.length()-1) );
        if (madeChanges) pinInBackground("needsSyncing");
    }

    public void setCycleType(String type) {
        List<String> add = new ArrayList<>();
        List<String> remove = new ArrayList<>();
        if (type.equalsIgnoreCase(CYCLE_TYPE_IVF)) {
            remove.add(CYCLE_TYPE_NATURAL);
            remove.add(CYCLE_TYPE_MEDICATED);
            remove.add(CYCLE_TYPE_IUI);
            add.add(CYCLE_TYPE_IVF);
        } else if (type.equalsIgnoreCase(CYCLE_TYPE_IUI)) {
            remove.add(CYCLE_TYPE_NATURAL);
            remove.add(CYCLE_TYPE_MEDICATED);
            add.add(CYCLE_TYPE_IUI);
            remove.add(CYCLE_TYPE_IVF);
        } else if (type.equalsIgnoreCase(CYCLE_TYPE_MEDICATED)) {
            remove.add(CYCLE_TYPE_NATURAL);
            add.add(CYCLE_TYPE_MEDICATED);
            remove.add(CYCLE_TYPE_IUI);
            remove.add(CYCLE_TYPE_IVF);
        } else {
            add.add(CYCLE_TYPE_NATURAL);
            remove.add(CYCLE_TYPE_MEDICATED);
            remove.add(CYCLE_TYPE_IUI);
            remove.add(CYCLE_TYPE_IVF);
        }
        updateTags(add,remove);
    }

    private List<String> getTagList() {
        List<String> tagList = new ArrayList<>();
        String tags = getTags();
        if (tags!=null && tags.length()>0) {
            if (tags.contains("|")) {
                String u[] = tags.split("\\|");
                tagList = new ArrayList<>( Arrays.asList(u) );
            } else {
                tagList.add(tags);
            }
        }
        return tagList;
    }

}