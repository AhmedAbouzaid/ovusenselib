package com.fertilityfocus.ovusenseapi.parse;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import java.util.Date;
import java.util.List;

/**
 * Created by Al Pirrie on 03/2018.
 */

@ParseClassName("UserMeta")
public class UserMeta extends ParseObject {

    public String getUserID() {
        return getString("userID");
    }
    public void setUserID(String value) {
        put("userID", value);
    }

    public String getUserPin() {
        return getString("userPin");
    }
    public void setUserPin(String value) {
        put("userPin", value);
    }

    public boolean getPinEnabled() {
        return getBoolean("pinEnabled");
    }
    public void setPinEnabled(boolean value) {
        put("pinEnabled", value);
    }

    public Date getDob() {
        return getDate("dob");
    }
    public void setDob(Date value) {
        put("dob", value);
    }

    public List<String> getSensorIDs() {
        return getList("sensorIDs");
    }
    public void setSensorIDs(List<String> value) {
        put("sensorIDs", value);
    }

    public boolean getNdefCapable() {
        return getBoolean("ndefCapable");
    }
    public void setNdefCapable(boolean value) {
        put("ndefCapable", value);
    }

    public String getTryingLength() {
        return getString("tryingLength");
    }
    public void setTryingLength(String value) {
        put("tryingLength", value);
    }

    public boolean getContactOptIn() {
        return getBoolean("contactOptIn");
    }
    public void setContactOptIn(boolean value) {
        put("contactOptIn", value);
    }

    public String getMagentoEmail() { return getString("magentoEmail"); }
    public void setMagentoEmail(String value) { put("magentoEmail", value); }

    public String getSubscriptionType() { return getString("subscriptionType"); }
    public void setSubscriptionType(String value) { put("subscriptionType", value); }

    public Date getSubscriptionCreated() { return getDate("subscriptionCreated");}
    public void setSubscriptionCreated(Date value) { put("subscriptionCreated", value); }

    public Date getSubscriptionExpires() { return getDate("subscriptionExpires");}
    public void setSubscriptionExpires(Date value) { put("subscriptionExpires", value); }

    public String getBillingSerial() { return getString("billingSerial"); }
    public void setBillingSerial(String value) { put("billingSerial", value); }

    public String getPreferredUnits() { return getString("preferredUnits"); }
    public void setPreferredUnits(String value) { put("preferredUnits", value); }

    public List<String> getPreferredEvents() { return getList("preferredEvents"); }
    public void setPreferredEvents(List<String> value) { put("preferredEvents", value); }

}
