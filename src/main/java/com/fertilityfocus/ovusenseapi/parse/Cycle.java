package com.fertilityfocus.ovusenseapi.parse;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by Al Pirrie on 03/2018.
 */

public class Cycle {
    public List<OvuRecording> recordings = new ArrayList<>();
    public String key = "";

    public static Cycle newInstance(String newKey) {
        Cycle newCycle = new Cycle();
        newCycle.key = newKey;
        newCycle.recordings = new ArrayList<>();
        return newCycle;
    }
}
