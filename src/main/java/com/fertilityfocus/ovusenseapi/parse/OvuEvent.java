package com.fertilityfocus.ovusenseapi.parse;

import com.fertilityfocus.ovusenseapi.ovusense.OvusenseLibraryWrapper;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import java.util.Date;

/**
 * Created by Al Pirrie on 03/2018.
 */

@ParseClassName("OvuEvent")
public class OvuEvent extends ParseObject {

    public static final String EVENT_TYPE_CYCLE_START = "cycle_start";
    public static final String EVENT_TYPE_CYCLE_END = "cycle_end";

    public static OvuEvent newInstance(Date startDate, String type, String value, String notes, String userId, boolean isVisible) {
        OvuEvent e = new OvuEvent();
        e.setEventDateUTC(startDate);
        e.setEventType(type);
        e.setValue(value);
        e.setNotes(notes);
        e.setOwnerUserID(userId);
        e.setVisible(isVisible);
        return e;
    }

    public static OvuEvent newInstance(Date startDate, String type, String strain, String value, String notes, String userId, boolean isVisible) {
        OvuEvent e = OvuEvent.newInstance(startDate, type, value, notes, userId, isVisible);
        e.setStrain(strain);
        return e;
    }

    public String debug() {
        String result = this.getEventType();
        if (this.getStrain()!=null && this.getStrain().length()!=0) result +=  ":" + this.getStrain();
        result += " (" + this.getOwnerUserID() + ")";
        result += " VALUE:" + this.getValue();
        result += " NOTES:" + this.getNotes();
        result += " @ " + this.getEventDateUTC();
        return result;
    }

    public String key() {
        String key = getEventType();
        String strain = getStrain();
        if (strain!=null && strain.length()>0) {
            key = key + ":" + strain;
        }
        return key;
    }

    public void clone(OvuEvent e) {
        if (e==null) return;
        if (e.getOwnerUserID()!=null) this.setOwnerUserID( e.getOwnerUserID() );
        if (e.getEventType()!=null) this.setEventType( e.getEventType() );
        if (e.getStrain()!=null) this.setStrain( e.getStrain() );
        if (e.getCycleUuid()!=null) this.setCycleUuid( e.getCycleUuid() );
        if (e.getEventDateUTC()!=null) this.setEventDateUTC( e.getEventDateUTC() );
        if (e.getUuid()!=null) this.setUuid( e.getUuid() );
        if (e.getValue()!=null) this.setValue( e.getValue() );
        if (e.getNotes()!=null) this.setNotes( e.getNotes() );
        this.setVisible( e.getVisible() );
    }

    public String getOwnerUserID() { return getString("ownerUserID"); }
    public void setOwnerUserID(String value) { put("ownerUserID", value); }

    public String getEventType() { return getString("eventType"); }
    public void setEventType(String value) { put("eventType", value); }

    public String getStrain() { return getString("strain"); }
    public void setStrain(String value) { put("strain", value); }

    public String getCycleUuid() { return getString("cycleUuid"); }
    public void setCycleUuid(String value) { put("cycleUuid", value); }

    public Date getEventDateUTC() { return getDate("eventDateUTC"); }
    public void setEventDateUTC(Date value) { put("eventDateUTC", value); }

    public String getUuid() { return getString("uuid"); }
    public void setUuid(String value) { put("uuid", value); }

    public String getValue() { return getString("value"); }
    public void setValue(String value) { put("value", value); }

    public String getNotes() { return getString("notes"); }
    public void setNotes(String value) { put("notes", value); }

    public boolean getVisible() { return getBoolean("visible"); }
    public void setVisible(boolean value) { put("visible", value); }

    /*
    public boolean getNeedsSyncing() { return getBoolean("needsSyncing"); }
    public void setNeedsSyncing(String value) { put("needsSyncing", value); }
    //*/

}