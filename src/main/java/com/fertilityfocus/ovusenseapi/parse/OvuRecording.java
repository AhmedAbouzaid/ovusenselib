package com.fertilityfocus.ovusenseapi.parse;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import java.util.Date;

/**
 * Created by Al Pirrie on 03/2018.
 */
@ParseClassName("OvuRecording")
public class OvuRecording extends ParseObject {

    public String getOwnerUserID() {
        return getString("ownerUserID");
    }
    public void setOwnerUserID(String value) {
        put("ownerUserID", value);
    }

    public Date getStartDateTimeUTC() {
        return getDate("startDateTimeUTC");
    }
    public void setStartDateTimeUTC(Date value) {
        put("startDateTimeUTC", value);
    }

    public String getCycleUuid() {
        return getString("cycleUuid");
    }
    public void setCycleUuid(String value) {
        put("cycleUuid", value);
    }

    public String getUuid() {
        return getString("uuid");
    }
    public void setUuid(String value) {
        put("uuid", value);
    }

    public String getInfo() {
        return getString("info");
    }
    public void setInfo(String value) {
        put("info", value);
    }

    public String getRecordingData() {
        return getString("recordingData");
    }
    public void setRecordingData(String value) {
        put("recordingData", value);
    }

    public double getCachedGraphX() {
        return getDouble("cachedGraphX");
    }
    public void setCachedGraphX(double value) {
        put("cachedGraphX", value);
    }

    public long getCachedGraphY() {
        return getLong("cachedGraphY");
    }
    public void setCachedGraphY(long value) {
        put("cachedGraphY", value);
    }

    public long getCachedSmoothY() {
        return getLong("cachedSmoothY");
    }
    public void setCachedSmoothY(long value) {
        put("cachedSmoothY", value);
    }

    public void clone(OvuRecording r) {
        if (r==null) return;
        if (r.getOwnerUserID()!=null) this.setOwnerUserID( r.getOwnerUserID() );
        if (r.getStartDateTimeUTC()!=null) this.setStartDateTimeUTC( r.getStartDateTimeUTC() );
        if (r.getCycleUuid()!=null) this.setCycleUuid( r.getCycleUuid() );
        if (r.getInfo()!=null) this.setInfo( r.getInfo() );
        if (r.getUuid()!=null) this.setUuid( r.getUuid() );
        if (r.getRecordingData()!=null) this.setRecordingData( r.getRecordingData() );
        this.setCachedGraphX( r.getCachedGraphX() );
        this.setCachedGraphY( r.getCachedGraphY() );
        this.setCachedSmoothY( r.getCachedSmoothY() );
    }
}