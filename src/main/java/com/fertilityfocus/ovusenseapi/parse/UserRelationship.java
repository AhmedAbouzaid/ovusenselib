package com.fertilityfocus.ovusenseapi.parse;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Al Pirrie on 03/2018.
 */

@ParseClassName("UserRelationship")
public class UserRelationship extends ParseObject {

    public String getParentID() {
        return getString("parentID");
    }
    public void setParentID(String value) {
        put("parentID", value);
    }

    public String getParentUsername() {
        return getString("parentUsername");
    }
    public void setParentUsername(String value) {
        put("parentUsername", value);
    }

    public String getChildID() {
        return getString("childID");
    }
    public void setChildID(String value) {
        put("childID", value);
    }

    public String getChildUsername() {
        return getString("childUsername");
    }
    public void setChildUsername(String value) {
        put("childUsername", value);
    }

}