package com.fertilityfocus.ovusenseapi.ovusense;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class ConnectionView extends View {

    private static int CONNECTION_VIEW_DEFAULT_COLOR = 0xff999999;
    private static int CONNECTION_VIEW_DEFAULT_ALPHA  = 0x7f;
    private static long CONNECTION_VIEW_DEFAULT_FRAMES = 20;
    private static float CONNECTION_VIEW_DEFAULT_2_PI = 6.2831854f;

    public boolean horizonal = true;
    public boolean animating = false;
    public long period = 1500;
    public int blobs = 3;
    public int blobColor;

    private float scaledDensity;
    private Paint blobPaint;
    private Timer timer;
    private float phi;
    private float minSize;
    private float maxSize;

    public ConnectionView(Context context) {
        super(context);
        setup();
    }

    public ConnectionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public ConnectionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup();
    }

    public void setup() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        scaledDensity = metrics.scaledDensity;

        phi = 0.f;
        period = 1500;
        blobs = 2;
        animating = false;
        blobColor = CONNECTION_VIEW_DEFAULT_COLOR;

        blobPaint = new Paint();
        blobPaint.setColor( CONNECTION_VIEW_DEFAULT_COLOR );
        blobPaint.setTextAlign(Paint.Align.CENTER);
        blobPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        timer = new Timer();
    }

    public void hide() {
        blobs = 0;
        postInvalidate();
    }

    public void update() {
        // Detect orientation
        horizonal = (this.getWidth() > this.getHeight());
        minSize = Math.min(this.getWidth(), this.getHeight()) / scaledDensity;
        maxSize = Math.max(this.getWidth(), this.getHeight()) / scaledDensity;

        // Reset blobs
        blobs = (int) Math.ceil( maxSize / (2.0 * minSize) );
        if (blobs<3) blobs = 3;
    }

    public void animate(boolean animate) {
        if (animate && !animating) {
            try {
                timer = new Timer();
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        timerDidFire();
                    }
                }, 0, 1000 / CONNECTION_VIEW_DEFAULT_FRAMES);
                animating = true;
            } catch (Exception e) {
                Log.d("ConnectionView","Error calling ANIMATE when status is OFF");
            }
        } else if (!animate && animating) {
            if (timer!=null) timer.cancel();
            animating = false;
            postInvalidate();
        }
    }

    private void timerDidFire() {
        long now = new Date().getTime();
        long t = now%period;
        phi = ((float)t / (float)period) * CONNECTION_VIEW_DEFAULT_2_PI;
        update();
        postInvalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (getContext()==null || canvas.getHeight() < 1 || blobs < 1) return;

        float x = canvas.getWidth() / 2f;
        float y = canvas.getHeight() / 2f;
        float delta = (scaledDensity * (maxSize - minSize)) / (float)(blobs - 1);

        for (int i=0; i<blobs; i++) {
            float c = animating ? (float) Math.cos(phi - (((float)i * CONNECTION_VIEW_DEFAULT_2_PI) / ((float)blobs * 3))) : 0;
            float alpha = CONNECTION_VIEW_DEFAULT_ALPHA * (1 + c);
            if (horizonal) {
                x = y + (i * delta);
            } else {
                y = x + (i * delta);
            }
            blobPaint.setAlpha((int)alpha);
            canvas.drawCircle(x, y, minSize, blobPaint);
        }

    }
}
