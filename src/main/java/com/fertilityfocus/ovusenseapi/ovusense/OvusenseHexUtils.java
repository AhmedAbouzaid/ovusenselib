package com.fertilityfocus.ovusenseapi.ovusense;

import android.util.Log;

/**
 * Created by Al Pirrie on 03/2018.
 */

public class OvusenseHexUtils {

    final private static char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static byte[] hexStringToByteArray(String s) {
        if (s==null) return new byte[0];
        int len = s.length();
        byte[] data = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }

    public static String byteArrayToHexString(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;

        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }

        return new String(hexChars);
    }

    public static String sensorSerialFromBytes(byte[] bytes) {
        try {
            return String.format("%c%c%c%c%c%c%c%c", bytes[8], bytes[9], bytes[10], bytes[11], bytes[12], bytes[13], bytes[14], bytes[15]);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("OvusenseHexUtils", "sensorSerialFromBytes ERROR: " + e);
            return "hexerror";
        }
    }

    public static String firmwareVersionFromBytes(byte[] bytes) {
        try {
            return String.format("%c%c%c%c", bytes[4], bytes[5], bytes[6], bytes[7]);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("OvusenseHexUtils", "firmwareVersionFromBytes ERROR: " + e);
            return "xxxx";
        }
    }
}
