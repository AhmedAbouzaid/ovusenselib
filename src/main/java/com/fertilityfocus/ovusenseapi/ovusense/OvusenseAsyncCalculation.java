package com.fertilityfocus.ovusenseapi.ovusense;

import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.fertilityfocus.ovusenseapi.parse.OvuCycle;
import com.fertilityfocus.ovusenseapi.parse.OvuRecording;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

public class OvusenseAsyncCalculation extends AsyncTask<String, Double, Integer> {

    private boolean calcRunning = false;
    private boolean calcPending = false;

    private int progressDone = 0;
    private int progressTotal = 1;
    private int progressSteps = 0;
    private int progressTotalSteps = 0;

    private String userID;
    private int algPredictedOvDay = 0;
    private int[] algReturnTime = {};
    private char algReturnStatus = 0;
    private ArrayList<ArrayList<Integer>> algRawData = new ArrayList<>();
    private ArrayList<ArrayList<Integer>> algSmoothData = new ArrayList<>();
    private final char EXPECTED = 0x80;
    private final char CONFIRMED = 0x40;
    private DateFormat ukDateFormatterShort = new SimpleDateFormat("EEE d MMM yyyy", Locale.UK);

    private final long ONE_DAY_IN_MILLISECONDS = 24L * 60L * 60L * 1000L;
    private final long THIRTEEN_HOURS_FORTY_MINUTES_IN_MILLISECONDS = ( (13L * 60L) + 40L ) * 60L * 1000L;
    private final long TIME_INTERVAL_HALF_RECORDING = THIRTEEN_HOURS_FORTY_MINUTES_IN_MILLISECONDS / 2L;
    private final long ONE_YEAR_IN_MILLISECONDS = ONE_DAY_IN_MILLISECONDS * 365L;

    /*
     DO NOT TRANSLATE THESE STRINGS - ALL CYCLE MESSAGES MUST BE STORED IN ENGLISH
     See MainFragment.java - private String processCycleMessage(final OvuCycle cycle, final int cycleNumber)
     */
    public final static String UK_FERTILE_WINDOW_WILL_START_FORMAT = "Your fertile window will start on %s.";
    public final static String UK_YOU_ARE_NOW_IN_YOUR_FERTILE_WINDOW = "You are now in your fertile window.";
    public final static String UK_OV_NOT_DETECTED_IN_CYCLE_X = "Ovulation was not detected in Cycle %d.";
    public final static String UK_WAITING_TO_CONFIRM_OVULATION = "Waiting to confirm ovulation in this cycle. Please continue to use your sensor.";
    public final static String UK_OV_CONFIRMED_STOP_USING_SENSOR = "Ovulation confirmed. You can stop using OvuSense for this cycle.";
    public final static String UK_YOU_OVULATED_ON_DAY_X_OF_CYCLE_Y_FORMAT = "You ovulated on Day %1$d of Cycle %2$d";
    public final static String UK_YOU_OVULATED_ON_DATE_X_FORMAT = "You ovulated on %s. You can stop using OvuSense for this cycle.";
    public final static String UK_YOU_ARE_NOW_IN_OV_WINDOW_OV_TOMORROW = "You are now in your ovulation window. Ovulation: tomorrow.";
    public final static String UK_YOU_ARE_NOW_IN_OV_WINDOW_OV_TODAY = "You are now in your ovulation window. Ovulation: today.";
    public final static String UK_YOU_ARE_NOW_IN_OV_WINDOW = "You are now in your ovulation window.";
    public final static String UK_OV_NOT_CONFIRMED_CONTINUE_USING_SENSOR = "Ovulation not confirmed. Please continue to use your sensor.";

    private String getTestString() { return testWrapper(); }

    public native String testWrapper();

    public native byte PredictOvDay(int[] jCycleStart, byte[] jPreviousOvDay, byte[] jAlgState);

    public native int AnalyseCycle(int[] jCycleStart, byte[] jPreviousOvDay, int[] jRecordingsStarts, char[] Ts);

    private void AnalyseCycleWrapped(Date cycleStart, List<Long> previousOvDays, List<OvuRecording> temperatures) {

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.clear();

        int[] nativeCycleStart = new int[3];
        calendar.setTime(cycleStart);
        nativeCycleStart[0] = calendar.get(Calendar.YEAR);
        nativeCycleStart[1] = calendar.get(Calendar.MONTH) + 1;
        nativeCycleStart[2] = calendar.get(Calendar.DAY_OF_MONTH);

        if (previousOvDays==null) previousOvDays = new ArrayList<>();
        byte[] nativePreviousOvDays = new byte[previousOvDays.size()];
        for (int i = 0; i < nativePreviousOvDays.length; i++) {
            nativePreviousOvDays[i] = previousOvDays.get(i).byteValue();
        }

        if (temperatures==null) temperatures = new ArrayList<>();
        int[] nativeRecordingStarts = new int[temperatures.size() * 3];
        for (int i = 0; i < nativeRecordingStarts.length; i += 3) {
            calendar.setTime(temperatures.get(i / 3).getStartDateTimeUTC());
            nativeRecordingStarts[i    ] = calendar.get(Calendar.YEAR);
            nativeRecordingStarts[i + 1] = calendar.get(Calendar.MONTH) + 1;
            nativeRecordingStarts[i + 2] = calendar.get(Calendar.DAY_OF_MONTH);
        }

        char[] nativeTemperatures = new char[temperatures.size() * 164];
        for (int i = 0; i < temperatures.size(); i++) {

            ArrayList<Integer> recordingTemperatures;
            try{
                recordingTemperatures = OvusenseLibraryWrapper.parseRecordingsToTemperatureArray(Base64.decode(temperatures.get(i).getRecordingData(), Base64.DEFAULT));
            }
            catch (Exception e){
                recordingTemperatures = new ArrayList<>();
            }

            for (int j = 0; j < 164; j++) {

                if (j < recordingTemperatures.size()) {
                    nativeTemperatures[i * 164 + j] = (char) (int) recordingTemperatures.get(j);
                } else {
                    nativeTemperatures[i * 164 + j] = 34500;
                }

                if (nativeTemperatures[i * 164 + j] < 34500) nativeTemperatures[i * 164 + j] = 34500;
            }
        }

        AnalyseCycle(nativeCycleStart, nativePreviousOvDays, nativeRecordingStarts, nativeTemperatures);
    }

    public void logFromJNI(String log){
        Log.d("OvuAsyncCalc/=========", log);
    }

    public void analyseDataFinished(int[] returnTime, char returnStatus, char[] graphData, char[] smoothData, int predictedOvDay) {

        algReturnTime = returnTime;
        algReturnStatus = returnStatus;
        algPredictedOvDay = predictedOvDay;

        ArrayList<Integer> returnedData = new ArrayList<>();
        for (char c : graphData) {
            returnedData.add((int)c);
        }
        algRawData.add(returnedData);

        ArrayList<Integer> returnedSmoothData = new ArrayList<>();
        for (char c : smoothData) {
            returnedSmoothData.add((int)c);
        }
        algSmoothData.add(returnedSmoothData);

    }

    private void incrementProgress() {
        progressSteps++;
        double progress = ((double) progressDone + ( (double)progressSteps / (double)progressTotalSteps ) ) / (double) progressTotal;
        if (progress<=1.0) publishProgress(progress, 0.0);
    }

    private void updateProgress(int done, int of, int steps) {
        progressDone = done;
        progressTotal = of;
        progressSteps = 0;
        progressTotalSteps = steps;
        double progress = (double) done / (double) of;
        if (progress<=1.0) publishProgress(progress, 0.0);
    }

    private void recalculateDataDone() {
        calcRunning = false;
        if (calcPending) {
            calcPending = false;
            doInBackground(userID);
        }
    }

    @Override
    protected Integer doInBackground(String... IDs) {
        userID = null;
        if (IDs.length>0) userID = IDs[0];
        if (userID == null) return 0;

        if (calcRunning) {
            calcPending = true;
            return 0;
        }
        calcRunning = true;
        ukDateFormatterShort.setTimeZone(TimeZone.getTimeZone("UTC"));

        ParseQuery<OvuCycle> ovuQuery = new ParseQuery<>(OvuCycle.class);
        ovuQuery.fromPin();
        ovuQuery.whereEqualTo("ownerUserID", userID);
        ovuQuery.orderByAscending("startDateTimeUTC");
        ovuQuery.addAscendingOrder("createdAt");
        ovuQuery.whereNotEqualTo("hidden", true);
        try {
            List<OvuCycle> cycleResults = ovuQuery.find();

            if (cycleResults==null || cycleResults.size() < 1) {
                return 1;
            }

            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            calendar.clear();
            boolean needsRecalculating = false;
            for (int index=0;index<cycleResults.size();index++) {
                OvuCycle cycle = null;
                try {
                    cycle = cycleResults.get(index);
                } catch (Exception e) {
                    Log.d("OvuAsyncCalc", "doInBackground EXCEPTION: " + e);
                }
                if (cycle==null) continue;
                Date cycleStartDate = new Date(cycle.getStartDateTimeUTC().getTime() + THIRTEEN_HOURS_FORTY_MINUTES_IN_MILLISECONDS);
                Date cycleEndDate;
                if (index+1 < cycleResults.size()) {
                    OvuCycle nextCycle = cycleResults.get(index+1);
                    cycleEndDate = new Date(nextCycle.getStartDateTimeUTC().getTime() + THIRTEEN_HOURS_FORTY_MINUTES_IN_MILLISECONDS);
                } else {
                    cycleEndDate = new Date( new Date().getTime() + ONE_YEAR_IN_MILLISECONDS );
                }

                boolean cycleIsCurrent = false;
                boolean cycleNeedsSyncing = cycle.getNeedsSyncing();

                if (index == cycleResults.size() - 1) {
                    cycleIsCurrent = true;
                    if (cycle.getLastCalculation() == null ||
                            OvusenseLibraryWrapper.calendarDaysBetweenDate(cycle.getLastCalculation(), OvusenseLibraryWrapper.timezonelessCurrentDatetime()) > 0) {
                        cycle.setNeedsRecalculating(true);
                    }
                }
                if (index > cycleResults.size() - 3)  cycle.setNeedsRecalculating(true);
                if (!cycle.getHasCalculatedSmoothResults()) cycle.setNeedsRecalculating(true);
                if (cycle.getNeedsRecalculating()) needsRecalculating = true;

                if (needsRecalculating) {
                    algReturnTime = new int[3];
                    algReturnStatus = 0;
                    algRawData = new ArrayList<>();
                    algSmoothData = new ArrayList<>();
                    cycle.setHasCalculatedSmoothResults(true);
                    cycle.setContainsSmoothData(false);

                    ArrayList<Long> previousDays = new ArrayList<>();
                    for (OvuCycle previousCycle : cycleResults) {
                        if (previousCycle.getStartDateTimeUTC().getTime() < cycle.getStartDateTimeUTC().getTime()
                                && !previousCycle.getOvulationNotConfirmed()) {
                            previousDays.add(previousCycle.getOvulationDay());
                        }
                    }

                    ParseQuery<OvuRecording> recQuery = new ParseQuery<>(OvuRecording.class);
                    recQuery.fromPin();
                    recQuery.whereGreaterThanOrEqualTo("startDateTimeUTC", cycleStartDate);
                    recQuery.whereLessThan("startDateTimeUTC", cycleEndDate);
                    recQuery.whereEqualTo("ownerUserID", userID);
                    recQuery.orderByAscending("startDateTimeUTC");
                    recQuery.addAscendingOrder("createdAt");
                    recQuery.setLimit(1000);
                    List<OvuRecording> recResults;
                    try {
                        recResults = recQuery.find();
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                        return 0;
                    }

                    updateProgress( index, cycleResults.size(), recResults.size() );

                    AnalyseCycleWrapped(cycle.getStartDateTimeUTC(), previousDays, recResults);

                    int AlgDay = algPredictedOvDay;
                    cycle.setOvulationDay(AlgDay);
                    cycle.setOvExpectedDay(0);
                    cycle.setOvAllowShift(false);
                    cycle.setOvulationNotConfirmed(true);

                    int DayInCycle = OvusenseLibraryWrapper.calendarDaysBetweenDate(cycle.getStartDateTimeUTC(), OvusenseLibraryWrapper.timezonelessCurrentDatetime()) + 1;

                    if (recResults.size() < 1) {
                        //* A special subset of ovulation messages if the user hasn't yet made a recording

                        int thisCycle = cycleResults.indexOf(cycle) + 1;
                        String cycleOvNotDetected = String.format(Locale.UK, UK_OV_NOT_DETECTED_IN_CYCLE_X, thisCycle);
                        calendar.setTime(cycle.getStartDateTimeUTC());
                        calendar.add(Calendar.DAY_OF_YEAR, AlgDay - 6);
                        String predictedFertileStartDate = ukDateFormatterShort.format(calendar.getTime());
                        String defaultNoOvMessage = (cycleIsCurrent ? UK_WAITING_TO_CONFIRM_OVULATION : cycleOvNotDetected);

                        cycle.setOvulationMessage(defaultNoOvMessage);
                        cycle.setShowFertileArea(false);
                        cycle.setShowOvulatingArea(false);
                        cycle.setShowFertileIndicator(false);
                        cycle.setShowOvulatingIndicator(false);
                        cycle.setShowOvulationLine(true);

                        if (AlgDay > 0) {
                            if (DayInCycle + 5 < AlgDay) {
                                cycle.setOvulationMessage(cycleIsCurrent ?
                                        String.format(UK_FERTILE_WINDOW_WILL_START_FORMAT,predictedFertileStartDate)
                                        : cycleOvNotDetected);
                                cycle.setShowFertileArea(true);
                                cycle.setShowOvulatingArea(false);
                                cycle.setShowFertileIndicator(false);
                                cycle.setShowOvulatingIndicator(false);
                            } else {
                                if (DayInCycle < AlgDay + 3) {
                                    cycle.setOvulationMessage(cycleIsCurrent ? UK_YOU_ARE_NOW_IN_YOUR_FERTILE_WINDOW : cycleOvNotDetected);
                                    cycle.setShowFertileArea(true);
                                    cycle.setShowOvulatingArea(false);
                                    cycle.setShowFertileIndicator(true);
                                    cycle.setShowOvulatingIndicator(false);
                                } else {
                                    cycle.setOvulationMessage(defaultNoOvMessage);
                                    cycle.setShowFertileArea(false);
                                    cycle.setShowOvulatingArea(false);
                                    cycle.setShowFertileIndicator(false);
                                    cycle.setShowOvulatingIndicator(false);
                                    AlgDay = 0;
                                }
                            }
                        }

                        cycle.setOvulationDay(AlgDay);
                        cycle.setNeedsRecalculating(false);

                        if (cycle.isDirty() || cycleNeedsSyncing) {
                            try {
                                cycle.setNeedsSyncing(false);
                                cycle.pin("needsSyncing");
                            } catch (ParseException e1) {
                                e1.printStackTrace();
                                return 0;
                            }
                        }

                        if (cycleIsCurrent) {
                            return 1;
                        }

                    } else {
                        //* Different messages if we actually have recordings

                        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                        c.clear();
                        c.set(algReturnTime[0], algReturnTime[1] - 1, algReturnTime[2]);
                        Date algDate = c.getTime();

                        int thisCycle = cycleResults.indexOf(cycle) + 1;
                        String cycleOvNotDetected = String.format(Locale.UK, UK_OV_NOT_DETECTED_IN_CYCLE_X, thisCycle);
                        String ovulationDate = ukDateFormatterShort.format(algDate);
                        calendar.setTime(cycle.getStartDateTimeUTC());
                        calendar.add(Calendar.DAY_OF_YEAR, AlgDay - 6);
                        String predictedFertileStartDate = ukDateFormatterShort.format(calendar.getTime());
                        String defaultNoOvMessage = (cycleIsCurrent ? UK_WAITING_TO_CONFIRM_OVULATION : cycleOvNotDetected);

                        cycle.setShowFertileArea(false);
                        cycle.setShowOvulatingArea(false);
                        cycle.setShowFertileIndicator(false);
                        cycle.setShowOvulatingIndicator(false);
                        cycle.setOvulationMessage(defaultNoOvMessage);

                        if (algReturnTime[0] > 1000) {
                            AlgDay = OvusenseLibraryWrapper.calendarDaysBetweenDate(cycle.getStartDateTimeUTC(), algDate) + 1;
                        }

                        for (int i=0; i<recResults.size(); i++) {

                            OvuRecording recording = recResults.get(i);
                            boolean changed = false;

                            if (!recording.getCycleUuid().equals(cycle.getUuid())) {
                                recording.setCycleUuid(cycle.getUuid());
                                changed = true;
                            }
                            Date endDate = new Date(recording.getStartDateTimeUTC().getTime() + THIRTEEN_HOURS_FORTY_MINUTES_IN_MILLISECONDS);
                            int RecEndDayInCycle = OvusenseLibraryWrapper.calendarDaysBetweenDate(cycle.getStartDateTimeUTC(), endDate);

                            ArrayList<Integer> graphPoints = algRawData.get(i);
                            ArrayList<Integer> smoothPoints = algSmoothData.get(i);

                            int pointIndex = OvusenseLibraryWrapper.calendarDaysBetweenDate(cycle.getStartDateTimeUTC(), recording.getStartDateTimeUTC());

                            if (pointIndex >= graphPoints.size()) {
                                pointIndex = graphPoints.size() - 1;
                            }

                            int graphY = graphPoints.get(pointIndex);
                            int smoothY = smoothPoints.get(pointIndex);
                            if (smoothY > 0) cycle.setContainsSmoothData(true);
                            int scanBackIndexLimit = Math.max(0, i-30);
                            for (int j=i;j>scanBackIndexLimit;j--) {
                                OvuRecording pastRec = recResults.get(j-1);
                                int pastPointIndexDifference = OvusenseLibraryWrapper.calendarDaysBetweenDate(pastRec.getStartDateTimeUTC(), recording.getStartDateTimeUTC());
                                if (pointIndex > pastPointIndexDifference) {
                                    int pastSmoothY = smoothPoints.get(pointIndex - pastPointIndexDifference);
                                    if (pastSmoothY > 0) {
                                        cycle.setContainsSmoothData(true);
                                        if (!recording.has("cachedSmoothY") || pastSmoothY!=pastRec.getCachedSmoothY()) {
                                            pastRec.setCachedSmoothY(pastSmoothY);
                                            pastRec.pin("needsSyncing");
                                        }
                                    }
                                }
                            }

                            calendar.setTime(endDate);

                            double graphX = calendar.get(Calendar.HOUR_OF_DAY) <= 12 ? RecEndDayInCycle - 0.5 : RecEndDayInCycle;
                            if (!recording.has("cachedGraphX") || graphX != recording.getCachedGraphX()) {
                                changed = true;
                                recording.setCachedGraphX(graphX);
                            }
                            if (!recording.has("cachedGraphY") || graphY!=recording.getCachedGraphY()) {
                                changed = true;
                                recording.setCachedGraphY(graphY);
                            }
                            if (!recording.has("cachedSmoothY") || (smoothY!=recording.getCachedSmoothY() && (smoothY!=0 || i>(recResults.size()-4)))) {
                                changed = true;
                                recording.setCachedSmoothY(smoothY);
                            }
                            incrementProgress();

                            if (recording.getCachedGraphY() < 1) {
                                Log.w("OvuAsyncCalc", "Problem with cachedGraphY. Day " + recording.getCachedGraphX() + "=" + recording.getCachedGraphY());
                            }

                            if (changed || cycleNeedsSyncing) {
                                try {
                                    recording.setUuid( UUID.randomUUID().toString() );
                                    recording.pin("needsSyncing");
                                } catch (ParseException e1) {
                                    e1.printStackTrace();
                                }
                            }

                        }

                        Date endDate = new Date(recResults.get(recResults.size() - 1).getStartDateTimeUTC().getTime() + THIRTEEN_HOURS_FORTY_MINUTES_IN_MILLISECONDS);
                        int RecEndDayInCycle = OvusenseLibraryWrapper.calendarDaysBetweenDate(cycle.getStartDateTimeUTC(), endDate);

                        cycle.setOvulationDay(AlgDay);

                        cycle.setShowOvulationLine(true);

                        if (AlgDay > 0) {
                            if ((algReturnStatus & EXPECTED) != 0) {
                                if ((algReturnStatus & CONFIRMED) != 0) {
                                    if (AlgDay < cycle.getOvExpectedDay()) {
                                        cycle.setOvulationMessage(cycleIsCurrent ?
                                                UK_OV_CONFIRMED_STOP_USING_SENSOR
                                                : String.format(Locale.UK, UK_YOU_OVULATED_ON_DAY_X_OF_CYCLE_Y_FORMAT, AlgDay, thisCycle));
                                        cycle.setShowFertileArea(false);
                                        cycle.setShowOvulatingArea(true);
                                        cycle.setShowFertileIndicator(false);
                                        cycle.setShowOvulatingIndicator(false);
                                        cycle.setOvulationNotConfirmed(false);
                                    } else {
                                        cycle.setOvulationMessage(cycleIsCurrent ?
                                                String.format(UK_YOU_OVULATED_ON_DATE_X_FORMAT, ovulationDate)
                                                : String.format(Locale.UK, UK_YOU_OVULATED_ON_DAY_X_OF_CYCLE_Y_FORMAT, AlgDay, thisCycle));
                                        cycle.setShowFertileArea(false);
                                        cycle.setShowOvulatingArea(true);
                                        cycle.setShowFertileIndicator(false);
                                        cycle.setShowOvulatingIndicator(false);
                                        cycle.setOvulationNotConfirmed(false);
                                    }
                                } else {
                                    if (DayInCycle + 1 == AlgDay) {
                                        cycle.setOvulationMessage(cycleIsCurrent ? UK_YOU_ARE_NOW_IN_OV_WINDOW_OV_TOMORROW : cycleOvNotDetected);
                                        cycle.setShowFertileArea(false);
                                        cycle.setShowOvulatingArea(true);
                                        cycle.setShowFertileIndicator(false);
                                        cycle.setShowOvulatingIndicator(true);
                                        cycle.setOvExpectedDay(AlgDay);
                                        cycle.setOvAllowShift(true);
                                    } else {
                                        if (DayInCycle == AlgDay) {
                                            cycle.setOvulationMessage(cycleIsCurrent ? UK_YOU_ARE_NOW_IN_OV_WINDOW_OV_TODAY  : cycleOvNotDetected);
                                            cycle.setShowFertileArea(false);
                                            cycle.setShowOvulatingArea(true);
                                            cycle.setShowFertileIndicator(false);
                                            cycle.setShowOvulatingIndicator(true);
                                            cycle.setOvExpectedDay(AlgDay);
                                            cycle.setOvAllowShift(true);
                                        } else {
                                            if (DayInCycle == AlgDay + 1 || DayInCycle == AlgDay + 2) {
                                                cycle.setOvulationMessage(cycleIsCurrent ? UK_YOU_ARE_NOW_IN_OV_WINDOW : cycleOvNotDetected);
                                                cycle.setShowFertileArea(false);
                                                cycle.setShowOvulatingArea(true);
                                                cycle.setShowFertileIndicator(false);
                                                cycle.setShowOvulatingIndicator(true);
                                            } else {
                                                if (DayInCycle == AlgDay + 3) {
                                                    cycle.setOvulationMessage(cycleIsCurrent ? UK_YOU_ARE_NOW_IN_OV_WINDOW : cycleOvNotDetected);
                                                    cycle.setShowFertileArea(false);
                                                    cycle.setShowOvulatingArea(true);
                                                    cycle.setShowFertileIndicator(false);
                                                    cycle.setShowOvulatingIndicator(true);

                                                    if (DayInCycle - RecEndDayInCycle == 0) {
                                                        if (cycle.getOvAllowShift()) {
                                                            AlgDay++;
                                                            cycle.setOvAllowShift(false);
                                                        } else {
                                                            cycle.setOvulationMessage(cycleIsCurrent ? UK_OV_NOT_CONFIRMED_CONTINUE_USING_SENSOR : cycleOvNotDetected);
                                                            cycle.setShowFertileArea(false);
                                                            cycle.setShowOvulatingArea(false);
                                                            cycle.setShowFertileIndicator(false);
                                                            cycle.setShowOvulatingIndicator(false);
                                                            cycle.setShowOvulationLine(false);
                                                            cycle.setOvulationDay(0);
                                                        }
                                                    }
                                                } else {
                                                    cycle.setOvulationMessage(cycleIsCurrent ? UK_OV_NOT_CONFIRMED_CONTINUE_USING_SENSOR : cycleOvNotDetected);
                                                    cycle.setShowFertileArea(false);
                                                    cycle.setShowOvulatingArea(false);
                                                    cycle.setShowFertileIndicator(false);
                                                    cycle.setShowOvulatingIndicator(false);
                                                    cycle.setShowOvulationLine(false);
                                                    cycle.setOvulationDay(0);
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (DayInCycle + 5 < AlgDay) {
                                    calendar.setTime(cycle.getStartDateTimeUTC());
                                    calendar.add(Calendar.DAY_OF_YEAR, AlgDay - 5);

                                    cycle.setOvulationMessage(cycleIsCurrent ?
                                            String.format(UK_FERTILE_WINDOW_WILL_START_FORMAT,predictedFertileStartDate)
                                            : cycleOvNotDetected);
                                    cycle.setShowFertileArea(true);
                                    cycle.setShowOvulatingArea(false);
                                    cycle.setShowFertileIndicator(false);
                                    cycle.setShowOvulatingIndicator(false);
                                } else {
                                    if (DayInCycle < AlgDay + 3) {
                                        cycle.setOvulationMessage(cycleIsCurrent ? UK_YOU_ARE_NOW_IN_YOUR_FERTILE_WINDOW : cycleOvNotDetected);
                                        cycle.setShowFertileArea(true);
                                        cycle.setShowOvulatingArea(false);
                                        cycle.setShowFertileIndicator(true);
                                        cycle.setShowOvulatingIndicator(false);
                                    } else {
                                        cycle.setOvulationMessage(defaultNoOvMessage);
                                        cycle.setShowFertileArea(false);
                                        cycle.setShowOvulatingArea(false);
                                        cycle.setShowFertileIndicator(false);
                                        cycle.setShowOvulatingIndicator(false);
                                        cycle.setOvulationDay(0);
                                    }
                                }
                            }
                        } else {
                            if ((algReturnStatus & EXPECTED) != 0) {
                                cycle.setOvulationMessage(cycleIsCurrent ? UK_OV_NOT_CONFIRMED_CONTINUE_USING_SENSOR : cycleOvNotDetected);
                                cycle.setShowFertileArea(false);
                                cycle.setShowOvulatingArea(false);
                                cycle.setShowFertileIndicator(false);
                                cycle.setShowOvulatingIndicator(false);
                                cycle.setShowOvulationLine(false);
                            }
                        }

                        cycle.setNeedsRecalculating(false);
                        cycle.setLastCalculation(OvusenseLibraryWrapper.timezonelessCurrentDatetime());

                        try {
                            cycle.setNeedsSyncing(false);
                            cycle.pin("needsSyncing");
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                            return 0;
                        }

                        if (cycleIsCurrent) {
                            return 1;
                        }
                    }
                } else { //does not require recalculation - use cached results

                    if (cycleResults!=null) updateProgress( index, cycleResults.size(), 0 );

                    ParseQuery<OvuRecording> recQuery = new ParseQuery<>(OvuRecording.class);
                    recQuery.fromPin();
                    recQuery.whereEqualTo("cycleUuid", cycle.getUuid());
                    recQuery.orderByAscending("startDateTimeUTC");
                    recQuery.addAscendingOrder("createdAt");
                    recQuery.setLimit(1000);
                    List<OvuRecording> recResults;
                    try {
                        recResults = recQuery.find();
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                        return 0;
                    }

                    for (int i=0;i<recResults.size(); i++) {
                        OvuRecording recording = recResults.get(i);
                        boolean shouldPinRecording = cycleNeedsSyncing;
                        if (!recording.getCycleUuid().equals(cycle.getUuid())) {
                            recording.setCycleUuid(cycle.getUuid());
                            shouldPinRecording = true;

                            cycle.setNeedsRecalculating(true);
                            cycle.setLastCalculation(null);
                            try {
                                cycle.pin("needsSyncing");
                            } catch (ParseException e1) {
                                e1.printStackTrace();
                                return 0;
                            }
                            calcPending = true;
                        } else if (i == recResults.size() - 1) {
                            if (cycleIsCurrent && recResults.get(i).getCachedGraphY() == 0) {
                                cycle.setNeedsRecalculating(true);
                                cycle.setLastCalculation(null);
                                try {
                                    cycle.pin("needsSyncing");
                                } catch (ParseException e1) {
                                    e1.printStackTrace();
                                    return 0;
                                }
                            }
                        }
                        if (shouldPinRecording) {
                            try {
                                recording.setUuid( UUID.randomUUID().toString() );
                                recording.pin("needsSyncing");
                            } catch (ParseException e1) {
                                e1.printStackTrace();
                            }
                        }

                    }

                    if (cycleIsCurrent) {
                        if (recResults==null || recResults.size()==0) {
                            cycle.setNeedsRecalculating(true);
                            cycle.setLastCalculation(null);
                            try {
                                cycle.pin("needsSyncing");
                            } catch (ParseException e1) {
                                e1.printStackTrace();
                                return 0;
                            }
                        }
                        return 1;
                    }

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

        return 1;
    }

    @Override
    protected void onProgressUpdate(Double... progress) {
        Double prog = progress[0];
        boolean isFinished = progress.length > 1 && progress[1] > 0.0;
    }

    @Override
    protected void onCancelled (Integer result) {
        Log.d("OvuAsyncCalc", "onCancelled: " + result);
    }

    @Override
    protected void onPostExecute(Integer result) {
        super.onPostExecute(result);
        recalculateDataDone();
    }
    
}
