package com.fertilityfocus.ovusenseapi.ovusense;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Al Pirrie on 03/2018.
 */

public class OvusenseLibraryWrapper {

    private static final long TWENTY_FOUR_HOURS_IN_MILLISECS = 86400000L;

    public static Date dateFromUniversalDayStamp(long ds) {
        return new Date(ds * TWENTY_FOUR_HOURS_IN_MILLISECS);
    }

    public static long universalDayStampForDate(Date d) {
        return d.getTime() / TWENTY_FOUR_HOURS_IN_MILLISECS;
    }

    public static float universalDayCodeForDate(Date d) {
        return (float)d.getTime() / (float)TWENTY_FOUR_HOURS_IN_MILLISECS;
    }

    public static Date cleanOffsetDate(long offset) {
        Date today = new Date();
        Date result = new Date( today.getTime() + (24L*60L*60L*1000L*offset) );
        return OvusenseLibraryWrapper.cleanDateFromDate(result);
    }

    public static Date cleanDateFromDate(Date d) {
        return dateFromUniversalDayStamp(universalDayStampForDate(d));
    }

    public static int dayInCycleStarting(Date cycleStartDate,  Date dayDate) {
        return calendarDaysBetweenDate(cycleStartDate, dayDate) + 1;
    }

    public static long timezonelessTimeSinceNow(Date d) {
        Date now = timezonelessCurrentDatetime();
        return (d.getTime() - now.getTime());
    }

    public static Date timezonelessCurrentDatetime() {

        Calendar utcCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        utcCalendar.clear();
        Calendar todayCalendar = Calendar.getInstance();
        todayCalendar.clear();
        Date todayDate = new Date();
        todayCalendar.setTime(todayDate);

        utcCalendar.set(Calendar.YEAR, todayCalendar.get(Calendar.YEAR));
        utcCalendar.set(Calendar.MONTH, todayCalendar.get(Calendar.MONTH));
        utcCalendar.set(Calendar.DAY_OF_YEAR, todayCalendar.get(Calendar.DAY_OF_YEAR));
        utcCalendar.set(Calendar.HOUR_OF_DAY, todayCalendar.get(Calendar.HOUR_OF_DAY));
        utcCalendar.set(Calendar.MINUTE, todayCalendar.get(Calendar.MINUTE));
        utcCalendar.set(Calendar.SECOND, todayCalendar.get(Calendar.SECOND));
        utcCalendar.set(Calendar.MILLISECOND, todayCalendar.get(Calendar.MILLISECOND));

        return utcCalendar.getTime();

    }

    public static int calendarDaysBetweenDate(Date startDateTimeUTC, Date now) {

        Calendar dayOne = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        dayOne.clear();
        dayOne.setTime(startDateTimeUTC);

        Calendar dayTwo = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        dayTwo.clear();
        dayTwo.setTime(now);


        if (dayOne.get(Calendar.YEAR) == dayTwo.get(Calendar.YEAR)) {
            return Math.abs(dayOne.get(Calendar.DAY_OF_YEAR) - dayTwo.get(Calendar.DAY_OF_YEAR));
        } else {
            if (dayTwo.get(Calendar.YEAR) > dayOne.get(Calendar.YEAR)) {
                //swap them
                Calendar temp = dayOne;
                dayOne = dayTwo;
                dayTwo = temp;
            }
            int extraDays = 0;

            int dayOneOriginalYearDays = dayOne.get(Calendar.DAY_OF_YEAR);

            while (dayOne.get(Calendar.YEAR) > dayTwo.get(Calendar.YEAR)) {
                dayOne.add(Calendar.YEAR, -1);
                extraDays += dayOne.getActualMaximum(Calendar.DAY_OF_YEAR);
            }

            return extraDays - dayTwo.get(Calendar.DAY_OF_YEAR) + dayOneOriginalYearDays;
        }

    }

    public static ArrayList<Integer> parseRecordingsToTemperatureArray(byte[] recordings) {

        ArrayList<Integer> retArray = new ArrayList<>();

        for (int i = 0; i < recordings.length; i += 2) {

            int recordingInt = 0;
            for (int j = 1; j >= 0; j--) { //backwards because little-endian
                recordingInt = (recordingInt << 8) | (recordings[i + j] & 0xFF);
            }

            int bigBits;
            int littleBits;
            bigBits = (recordingInt >> 10) & ~(~0 << 6);
            littleBits = recordingInt & ~(~0 << 10);
            Integer retNumber = bigBits * 1000 + littleBits;

            if (retNumber < 34500) {
                retNumber = 34500;
            }
            if (retNumber > 40000) {
                retNumber = 40000;
            }

            retArray.add(retNumber);

        }

        return retArray;


    }

}
