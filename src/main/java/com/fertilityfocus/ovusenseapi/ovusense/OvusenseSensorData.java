package com.fertilityfocus.ovusenseapi.ovusense;

import android.util.Base64;
import android.util.Log;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class OvusenseSensorData {

    private int ntagIndex;
    private int totalRecords;

    private String sensorSerial;
    private String sensorUDID;
    private String hardwareVersion;
    private String firmwareVersion;
    private String recordingBase64Encoded;
    private List<Byte> temperatures;
    private List<Byte> ntagIndexInfo;
    private List<Byte> rate;
    private List<Byte> battery;
    private List<Byte> recordHeader;
    private List<Byte> recordingData;
    private List<Integer> recording;

    public List<Byte> status;
    public ArrayList<Byte> recordingArray;

    private static String stringFromByteRange(byte[] b, int first, int length) {
        StringBuilder stringBuilder = new StringBuilder();
        int max = b.length - first;
        for (int i=0; i<length && i<max; i++) {
            try {
                stringBuilder.append(String.format("%c", b[i + first]));
            } catch (Exception e) {
                Log.d("OvusenseSensorData", "illegal character from byte: "+ (i+first) + " = " + b[i + first]);
                stringBuilder.append("?");
            }
        }
        return stringBuilder.toString();
    }

    private static List<Byte> byteListFromArrayRange(byte[] b, int first, int length) {
        List<Byte> ba = new ArrayList<>();
        int max = b.length - first;
        for (int i=0; i<length && i<max; i++) {
            ba.add(b[i+first]);
        }
        return ba;
    }

    private static byte[] byteArrayFromList(List<Byte> list){
        if (list==null) return new byte[0];
        byte[] ret = new byte[list.size()];
        for(int i = 0; i<ret.length; i++)
            ret[i] = list.get(i);
        return ret;
    }

    private static int[] intArrayFromList(List<Integer> list){
        if (list==null) return new int[0];
        int[] ret = new int[list.size()];
        for(int i = 0;i < ret.length;i++)
            ret[i] = list.get(i);
        return ret;
    }

    public static OvusenseSensorData newInstance(byte[] b) {
        OvusenseSensorData sensorData = new OvusenseSensorData();

        sensorData.hardwareVersion =    String.format(Locale.UK, "%d%d", b[0], b[1]);

        sensorData.firmwareVersion =    stringFromByteRange(b, 2, 4);       // 2-5
        sensorData.sensorSerial =       stringFromByteRange(b, 6, 8);       // 6-13
        sensorData.sensorUDID =         stringFromByteRange(b, 650, 16);    // 650-665

        sensorData.temperatures =       byteListFromArrayRange(b, 14, 592); // 14-605
        sensorData.ntagIndexInfo =      byteListFromArrayRange(b, 606, 4);  // 606-609
        sensorData.status =             byteListFromArrayRange(b, 614, 4);  // 614-617
        sensorData.rate =               byteListFromArrayRange(b, 618, 2);  // 618-619
        sensorData.battery =            byteListFromArrayRange(b, 620, 2);  // 620-621
        sensorData.recordHeader =       byteListFromArrayRange(b, 742, 16); // 742-757

        byte[] ntagIndexBytes = { sensorData.ntagIndexInfo.get(1), sensorData.ntagIndexInfo.get(0) };
        sensorData.ntagIndex = new BigInteger(1, ntagIndexBytes).intValue();
        byte[] recordsBytes = { sensorData.recordHeader.get(11), sensorData.recordHeader.get(10), sensorData.recordHeader.get(9), sensorData.recordHeader.get(8) };
        sensorData.totalRecords = new BigInteger(1, recordsBytes).intValue();

        sensorData.recordingArray = new ArrayList<>();
        int cutOff = sensorData.ntagIndex * 2;
        for (int i=cutOff; i<sensorData.temperatures.size(); i++) {
            sensorData.recordingArray.add(sensorData.temperatures.get(i));
        }
        for (int i=0; i<cutOff; i++) {
            sensorData.recordingArray.add(sensorData.temperatures.get(i));
        }
        sensorData.recordingData = sensorData.recordingArray;

        byte[] byteArray = new byte[592];
        for (int i = 0; i < byteArray.length; i++) {
            if (i < sensorData.recordingArray.size()) {
                byteArray[i] = sensorData.recordingArray.get(i);
            } else {
                byteArray[i] = 0;
            }
        }
        sensorData.recordingBase64Encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);

        List<Integer> recording;
        try {
            recording = OvusenseLibraryWrapper.parseRecordingsToTemperatureArray(Base64.decode(sensorData.recordingBase64Encoded, Base64.DEFAULT));
        } catch (Exception e){
            recording = new ArrayList<>();
        }
        sensorData.recording = recording;

        return sensorData;
    }

    public int recordsAvailable() {
        if (recordHeader==null || recordHeader.size()<12) return -1;
        byte[] interestingBytes = {recordHeader.get(11), recordHeader.get(10), recordHeader.get(9), recordHeader.get(8)};
        return new BigInteger(1, interestingBytes).intValue();
    }

}
