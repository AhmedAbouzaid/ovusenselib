package com.fertilityfocus.ovusenseapi.ovusense;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.fertilityfocus.ovusenseapi.R;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Al Pirrie on 03/2018.
 */

public class OvusensePrefs {

    private static final String PREFS_NAME = "com.fertilityfocus.ovusenseapi.preferences";

    public static final int SENSOR_OK = 0;
    public static final int SENSOR_WARN = 1;
    public static final int SENSOR_CRITICAL = 2;
    public static final int SENSOR_EXPIRED = 3;
    public static final int SENSOR_MISSING = 4;

    private static final int GRAPH_TYPE_RAW = 0;
    private static final int GRAPH_TYPE_SMOOTH = 1;
    private static final int GRAPH_TYPE_BOTH = 2;

    private static Boolean firmwareIsOnOrAfter(String firmwareVersion, int cutoff) {
        if (firmwareVersion==null || firmwareVersion.length()<1) return false;
        boolean isOnOrAfter = false;
        try {
            int code = Integer.parseInt(firmwareVersion);
            if (code >= cutoff) isOnOrAfter = true;
        } catch (NumberFormatException e) {
            // Contains special characters - must be old
            StringBuilder justNumbers = new StringBuilder();
            String numbers = "0123456789";
            for (int i=0;i<firmwareVersion.length();i++) {
                char letter = firmwareVersion.charAt(i);
                if (numbers.indexOf(letter)>=0) justNumbers.append(letter);
            }
            // Try again with just numbers
            try {
                int newcode = Integer.parseInt(justNumbers.toString());
                if (newcode >= cutoff) isOnOrAfter = true;
            } catch (NumberFormatException e2) {
                // Must be empty or broken - default to old method
                return false;
            }
        }
        return isOnOrAfter;
    }

    public static Boolean canReadDataFromNTAG(String firmwareVersion) {
        return OvusensePrefs.firmwareIsOnOrAfter(firmwareVersion, 500);
    }

    public static Boolean canReadDataFromNDEF(String firmwareVersion) {
        return OvusensePrefs.firmwareIsOnOrAfter(firmwareVersion, 600);
    }

    public static void deleteKeychainAndDefaultsData(Context c) {
        if (c==null) return;
        c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().clear().apply();
    }

    public static String getSelectedUserID(Context c) {
        if (c==null) return null;
        return c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).getString("selected_user_id", null);
    }

    public static void setSelectedUserID(Context c, String userid) {
        if (c==null) return;
        c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().putString("selected_user_id", userid).apply();
    }

    public static boolean registeredSensorIsNDEFCapable(Context c) {
        if (c==null) return false;
        return c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).getBoolean("sensor_is_ndef_capable", false);
    }

    public static void setRegisteredSensorIsNDEFCapable(Context c, boolean isNDEFCapable) {
        if (c==null) return;
        c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().putBoolean("sensor_is_ndef_capable", isNDEFCapable).apply();
    }

    public static List<String> getCachedRegisteredSensors(Context c) {
        if (c==null) return new ArrayList<>();
        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).getString("cached_registered_sensors", ""));
        } catch (Exception e) {
            return null;
        }
        ArrayList<String> retList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            String s = jsonArray.optString(i);
            if (s != null) {
                retList.add(s);
            }
        }
        return retList;
    }

    public static void setCachedRegisteredSensors(Context c, List<String> registeredSensors) {
        if (c==null) return;
        JSONArray jsonArray = new JSONArray();
        if (registeredSensors != null) {
            for (Object s : registeredSensors) {
                jsonArray.put("" + s);
            }
        }
        c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().putString("cached_registered_sensors", jsonArray.toString()).apply();
    }


    public static String getPreferredUnits(Context c) {
        String temp = getDisplayTemperatureUnit(c);
        return "" + temp;
    }

    public static void setPreferredUnits(Context c, String preferredUnits) {
        if (preferredUnits==null) return;
        String[] units = preferredUnits.split("\\|");
        if (units.length>0) setTemperatureIsFahrenheit(c, units[0].equals("F"));
    }

    public static boolean getTemperatureIsFahrenheit(Context c) {
        if (c==null) return false;
        return c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).getBoolean("is_Fahrenheit", false);
    }

    public static void setTemperatureIsFahrenheit(Context c, boolean isFahrenheit) {
        if (c==null) return;
        c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().putBoolean("is_Fahrenheit", isFahrenheit).apply();
    }

    public static int getPreferredGraphType(Context c) {
        if (c==null) return GRAPH_TYPE_BOTH;
        return c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).getInt("preferred_graph_type", GRAPH_TYPE_BOTH);
    }

    public static int togglePreferredGraphType(Context c) {
        if (c==null) return GRAPH_TYPE_BOTH;
        int preferredType = getPreferredGraphType(c);
        preferredType++;
        if (preferredType > GRAPH_TYPE_RAW && preferredType > GRAPH_TYPE_SMOOTH && preferredType > GRAPH_TYPE_BOTH) preferredType = GRAPH_TYPE_RAW;
        setPreferredGraphType(c, preferredType);
        return preferredType;
    }

    public static void setPreferredGraphType(Context c, int graphType) {
        if (c==null) return;
        if (graphType!=GRAPH_TYPE_RAW && graphType!=GRAPH_TYPE_SMOOTH && graphType!=GRAPH_TYPE_BOTH) graphType = GRAPH_TYPE_RAW;
        c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().putInt("preferred_graph_type", graphType).apply();
    }

    public static String getDisplayTemperatureUnit(Context c) {
        return getTemperatureIsFahrenheit(c) ? "F" : "C";
    }

    public static String getDisplayTemperature(Context c, float originalTemperature, boolean showUnits, int decimalPlaces) {
        if (originalTemperature < 34.5 || originalTemperature > 42) {
            if (c==null) return "";
        }

        double temp = getTemperatureIsFahrenheit(c) ? ((originalTemperature * 1.8) + 32) : originalTemperature;
        String format = String.format(Locale.UK,"%%.0%df",decimalPlaces);
        String units = showUnits ? (getTemperatureIsFahrenheit(c)?" °F":" °C") : "";
        return String.format(format, temp) + units;

    }

    public static Date getLastUpdate(Context c, String thingUpdated, String userID) {
        if (c==null) return new Date(0);
        String key = "updated_"+thingUpdated+"_"+userID;
        return new Date(c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).getLong(key, 0));
    }

    public static void logLastUpdate(Context c, String thingUpdated, String userID) {
        setLastUpdate(c, thingUpdated, userID, new Date());
    }

    private static void setLastUpdate(Context c, String thingUpdated, String userID, Date syncDate) {
        if (c==null) return;
        String key = "updated_"+thingUpdated+"_"+userID;
        c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().putLong(key, syncDate.getTime()).apply();
    }

    /*
     * Sensors
     */

    public static long getDaysRemainingForSensor(Context c, String sensor) {
        if (c==null) return 0;
        return c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).getLong(String.format("sensor_days_remaining_%s", sensor), 0);
    }

    public static void setDaysRemaining(Context c, long days, String sensor) {
        if (c==null) return;
        c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().putLong(String.format("sensor_days_remaining_%s", sensor), days).apply();
    }

    public static Date getCachedExpiryDateForSensor(Context c, String sensor) {
        if (c==null) return new Date(0);
        return new Date(c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).getLong(String.format("sensor_expiry_%s", sensor), 0));
    }

    public static void setCachedExpiryDate(Context c, Date date, String sensor) {
        if (c==null) return;
        c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().putLong(String.format("sensor_expiry_%s", sensor), date.getTime()).apply();
    }

    public static long getMaxSensorDaysRemaining(Context c) {
        List<String> sensorArray = getCachedRegisteredSensors(c);
        long maxDaysRemaining = -99998;
        if (sensorArray==null || sensorArray.size()==0) return -99999;
        for (String sensorToCheck : sensorArray) {
            long daysLeft = getDaysRemainingForSensor(c, sensorToCheck);
            if (daysLeft > maxDaysRemaining) maxDaysRemaining = daysLeft;
        }
        return maxDaysRemaining;
    }

    public static int getSensorStatus(Context c) {
        long maxDaysRemaining = getMaxSensorDaysRemaining(c);
        if (maxDaysRemaining == -99999) return SENSOR_MISSING;
        if (maxDaysRemaining == -99998) return SENSOR_OK;
        if (maxDaysRemaining < 1) return SENSOR_EXPIRED;
        if (maxDaysRemaining < 30) return SENSOR_WARN;
        return SENSOR_OK;
    }

    public static Date getLastSuccessfulInternetCheck(Context c) {
        if (c==null) return new Date(0);
        return new Date(c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).getLong("last_successful_internet_check", 0));
    }

    public static void setLastSuccessfulInternetCheck(Context c, Date date) {
        if (c==null) return;
        c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().putLong("last_successful_internet_check", date.getTime()).apply();
    }

    public static boolean getCachedMetaCompleted(Context c) {
        if (c==null) return false;
        return c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).getBoolean("cached_meta_completed", false);
    }

    public static void setCachedMetaCompleted(Context c, boolean isCompleted) {
        if (c==null) return;
        c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().putBoolean("cached_meta_completed", isCompleted).apply();
    }

    public static String getCachedMagentoEmail(Context c){
        if (c==null) return null;
        return c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).getString("cached_magento_email", null);
    }

    public static void setCachedMagentoEmail(Context c, String email){
        if (c==null || email==null) return;
        c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().putString("cached_magento_email", email).apply();
    }

    public static Date getCachedSubscriptionCreated(Context c){
        if (c==null) return new Date(0);
        return new Date(c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).getLong("cached_subscription_created", 0));
    }

    public static void setCachedSubscriptionCreated(Context c, Date date){
        if (c==null) return;
        if(date == null){
            c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().remove("cached_subscription_created").apply();
        } else {
            c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().putLong("cached_subscription_created", date.getTime()).apply();
        }
    }

    public static Date getCachedSubscriptionExpires(Context c){
        if (c==null) return new Date(0);
        return new Date(c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).getLong("cached_subscription_expires", 0));
    }

    public static void setCachedSubscriptionExpires(Context c, Date date) {
        if (c==null) return;
        if(date == null){
            c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().remove("cached_subscription_expires").apply();
        } else {
            c.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().putLong("cached_subscription_expires", date.getTime()).apply();
        }
    }

}
